#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
# https://hub.docker.com/_/microsoft-dotnet-aspnet

# datos del Usuario
ARG USUARIO=huezo
ARG UID=1990
ARG GID=1990
# Clave para el Usuario
ARG CLAVE=huezo
#Creacion Usuario
RUN useradd -m ${USUARIO} --uid=${UID} && echo "${USUARIO}:${CLAVE}" | chpasswd



WORKDIR /app
EXPOSE 80
EXPOSE 443

RUN chown -R huezo:huezo /app 
RUN chmod 755 /app 

ENV ASPNETCORE_URLS=http://+:8181
EXPOSE 8181

ENV TZ=America/El_Salvador
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update


ENV DOTNET_HOSTBUILDER__RELOADCONFIGONCHANGE=false


RUN apt-get update &&  apt-get install -y apt-utils libgdiplus libc6-dev libc6 build-essential
RUN ln -s /lib64/libdl.so.2 /lib64/libdl.so

COPY . .
WORKDIR /app
RUN ls -ltra

RUN sed -i'.bak' 's/$/ contrib/' /etc/apt/sources.list

# If you want to use Microsoft fonts in reports, you must install the fonts
# Andale Mono, Arial Black, Arial, Comic Sans MS, Courier New, Georgia, Impact,
# Times New Roman, Trebuchet, Verdana,Webdings)
RUN echo "ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true" | debconf-set-selections
# RUN apt-get install -y --no-install-recommends fontconfig ttf-mscorefonts-installer

RUN apt-get install -y  /app/ttf-mscorefonts-installer_3.7_all.deb

USER huezo



ENTRYPOINT ["dotnet", "htc.ea.saf.security.dll"]
