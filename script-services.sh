IMAGEN=$(basename $PWD) 

TAG=2021

#QA
#PUERTO=8092

#NUBE
PUERTO=8082
#build

docker build -t $IMAGEN:$TAG .

#status rm
if docker service ps $IMAGEN ; then
    echo -e $'\n\t\tService is running already... removing '$nombrecontenedor'...\n'
    docker service rm $IMAGEN && sleep 5 
else
   echo -e $'*************************************\n'
   echo -e $'\n\t\tService not active, starting...\n' 
fi
#swarm
docker service create --name=$IMAGEN --restart-condition=on-failure --publish=$PUERTO:80 --constraint=node.role==manager --mount=type=bind,src=/etc/localtime,target=/etc/localtime $IMAGEN:$TAG
